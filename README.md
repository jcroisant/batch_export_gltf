# Batch Export glTF 2.0

A simple Blender add-on to quickly export
multiple collections as glTF files (.glb/.gltf)
for use in the Godot game engine, etc.
It uses the glTF 2.0 export add-on that comes with Blender.

If you found this add-on useful please take a moment to add
a nice comment or :heart: the
[appreciation issue](https://gitlab.com/jcroisant/batch_export_gltf/issues/1).


## Usage

1. Download `batch_export_gltf.py` and
   [install it as an add-on in Blender](https://docs.blender.org/manual/en/latest/editors/preferences/addons.html#rd-party-add-ons).

2. In your Blender scene, create one collection per export file.
   Name the collection e.g. `Export: player`, to export to a file
   named `player.glb` (or `player.gltf` depending on saved glTF 2.0
   exporter settings). Only collections starting with `Export:` will
   be exported.

3. Move the objects you want to export into each collection.
   Everything in a collection will be exported to the same file.

4. Optionally add custom properties to objects (see below).

5. Run `File > Export > Batch glTF 2.0 (.glb/.gltf)`.
   The files will be exported relative to the directory that the
   current `.blend` file is saved in.

If you have previously used the glTF 2.0 exporter with the
`Remember Export Settings`, this script will use those settings,
except for `Include > Selected Objects` which will always be enabled.
Otherwise the defaults from the glTF 2.0 export add-on are used.

These custom properties are recognized on objects (but not bones):

- `export_exclude`: If property exists, don't export the object.
- `export_reset_loc`: If property exists, temporarily reset the
  object's location during export.
- `export_reset_rot`: If property exists, temporarily reset the
  object's rotation during export.
- `export_reset_scale`: If property exists, temporarily reset the
  object's scale during export.


## License (MIT)

Copyright © 2020 John Croisant

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation files
(the “Software”), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

